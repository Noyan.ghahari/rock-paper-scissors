import React, { Component } from "react";
import Modal from "./component/Modal.js";
import "./css/global.scss";

import Rock from "./asset/icons/unnamed.png";
import paper from "./asset/icons/paper.png";
import scissor from "./asset/icons/scissor.png";
import pcPlayer from "./asset/icons/question.png";

const option = [Rock, paper, scissor];

export default class App extends Component {
  state = {
    show: false,
    pcChoose: null,
    userChoose: null,
    result: [],
    userScore: 0,
    pcScore: 0,
  };
  showPcChoose = () => {
    var x = Math.floor(Math.random() * 3);
    this.setState({
      pcChoose: x,
    });
  };

  showMychoose = (userChoose) => {
    var tempUserChoose = null;
    switch (userChoose) {
      case "rock":
        tempUserChoose = 0;
        break;
      case "paper":
        tempUserChoose = 1;
        break;
      case "scissor":
        tempUserChoose = 2;
        break;

      default:
        break;
    }
    this.setState({ userChoose: tempUserChoose }, () => {
      this.showResult();
      // this.gameScore();

      console.log("123");
    });
  };

  showResult = () => {
    if (this.state.userChoose === 0) {
      if (this.state.pcChoose === 0) {
        this.setState({
          result: ["DRAW", "blue"],
        });
      }
      if (this.state.pcChoose === 1) {
        this.setState({
          result: ["YOU LOSSE", "red"],
        });
      }
      if (this.state.pcChoose === 2) {
        this.setState({
          result: ["YOU WIN", "green"],
        });
      }
    }
    ///////////////////////////////////////////////////////////////////
    if (this.state.userChoose === 1) {
      if (this.state.pcChoose === 0) {
        this.setState({
          result: ["YOU WIN", "green"],
        });
      }
      if (this.state.pcChoose === 1) {
        this.setState({
          result: ["DRAW", "blue"],
        });
      }
      if (this.state.pcChoose === 2) {
        this.setState({
          result: ["YOU LOSSE", "red"],
        });
      }
    }
    ////////////////////////////////////////////////////////////////////
    if (this.state.userChoose === 2) {
      if (this.state.pcChoose === 0) {
        this.setState({
          result: ["YOU LOSSE", "red"],
        });
      }
      if (this.state.pcChoose === 1) {
        this.setState({
          result: ["YOU WIN", "green"],
        });
      }
      if (this.state.pcChoose === 2) {
        this.setState({
          result: ["DRAW", "blue"],
        });
      }
    }
  };
  gameScore = () => {
    if (this.state.result[0] === "YOU WIN") {
      this.setState({
        userScore: this.state.userScore + 1,
      });
    }
    if (this.state.result[0] === "YOU LOSSE") {
      this.setState({
        pcScore: this.state.pcScore + 1,
      });
    }
  };
  showModal = () => {
    this.showPcChoose();
    this.setState({
      show: true,
    });
  };
  closeModal = () => {
    this.setState({
      show: false,
    });
    this.gameScore();
  };
  render() {
    return (
      <React.Fragment>
        <div className="main-container">
          <div className="scoreboard-container scoreboard-container-mob ">
            <div className="user-scoreboard-element-cont user-scoreboard-element-mob-cont">
              <div className="scoreboard-element scoreboard-element-mob">
                <div className="user-score user-score-mob">
                  <div>
                    <h1 style={{ color: "white" }}>Wins</h1>
                  </div>
                  <div className="userScore">
                    <h1>{this.state.userScore}</h1>
                  </div>
                </div>
              </div>
            </div>
            <div className="pc-scoreboard-element-cont">
              <div className="pc-score pc-score-mob">
                <div>
                  <h1 style={{ color: "white" }}>Losses</h1>
                </div>
                <div className="pcScore">
                  <h1>{this.state.pcScore}</h1>
                </div>
              </div>
            </div>
          </div>
          <div className="hands-pc-container hands-pc-container-mob">
            <div className="pc-img pc-img-mob">
              <img className="pcIcon" src={pcPlayer} alt="pc "></img>
            </div>
            <div className="hands-img hands-img-mob">
              <img
                className="userIcon"
                src={Rock}
                alt="rock"
                onClick={() => {
                  this.showMychoose("rock");
                  this.showModal();
                }}
              ></img>
              <img
                className="userIcon"
                src={paper}
                alt="paper"
                onClick={() => {
                  this.showMychoose("paper");
                  this.showModal();
                }}
              ></img>
              <img
                className="userIcon"
                src={scissor}
                contin
                alt="scissor"
                onClick={() => {
                  this.showMychoose("scissor");
                  this.showModal();
                }}
              ></img>
            </div>
          </div>
        </div>
        <Modal
          show={this.state.show}
          onClose={this.closeModal}
          MyChoose={option[this.state.userChoose]}
          choose={option[this.state.pcChoose]}
          GameResult={this.state.result[0]}
          color={this.state.result[1]}
        ></Modal>
      </React.Fragment>
    );
  }
}
