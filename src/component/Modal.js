import React, { Component } from "react";

export default class Modal extends Component {
  render() {
    if (!this.props.show) {
      return null;
    }

    return (
      <React.Fragment>
        <div className="myModal">
          {/* {console.log(this.props)} */}
          <div className="modal-content">
            <div className="modal-content-elements">
              <div className="pc-choose-img-container">
                <div className="choose-container">
                  <div>
                    <h2 className="pc-cont">PC</h2>
                    <img
                      className="pc-choose-img"
                      src={this.props.choose}
                      alt={`img ${this.props.choose}`}
                    ></img>
                  </div>
                  <div>
                    <div></div>
                    <h2 className="you-cont">YOU</h2>
                    <img
                      className="user-choose-img"
                      src={this.props.MyChoose}
                      alt={`img ${this.props.MyChoose}`}
                    ></img>
                  </div>
                </div>
              </div>
              <div className="result-container">
                {console.log(this.props)}
                <h2 className="result-text" style={{ color: this.props.color }}>
                  {this.props.GameResult}...!
                </h2>
              </div>
              <div className="button-container">
                <button className="button-style" onClick={this.props.onClose}>
                  continue
                </button>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
